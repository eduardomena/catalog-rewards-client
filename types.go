package catalog_rewards

const (
	APIBase     = "http://localhost"
	APISandbox  = "https://qa.api.catalogo.alus.com.mx"
	APIBaseLive = "https://catalogos.api.regalus.com.mx"
)

type CategoriesResponse struct {
	StatusCode uint `json:"status_code"`
	Data       []CategoryItem
	Message    string `json:"message,omitempty"`
}

type CategoryItem struct {
	ID    uint                `json:"id"`
	Name  string              `json:"name"`
	Child []CategoryItemChild `json:"child"`
	Image []string            `json:"image"`
}

type CategoryItemChild struct {
	ID       uint     `json:"id"`
	ParentID uint     `json:"parent_id"`
	Name     string   `json:"name"`
	Image    []string `json:"image"`
}

type ProductsResponse struct {
	StatusCode uint `json:"status_code"`
	Data       []ProductItem
	Message    string `json:"message,omitempty"`
}

type ProductResponse struct {
	StatusCode uint        `json:"status_code"`
	Message    interface{} `json:"message,omitempty"`
	Product    ProductItem
}

type ProductItem struct {
	ID               uint                `json:"id"`
	PartNumber       string              `json:"part_number"`
	Price            float64             `json:"price"`
	PointsValue      float64             `json:"points_value"`
	ConvertionFactor float32             `json:"convertion_factor"`
	Type             string              `json:"type"`
	Status           string              `json:"status"`
	Stock            uint16              `json:"stock"`
	Name             string              `json:"name"`
	Description      string              `json:"description"`
	Vendor           ProductItemVendor   `json:"vendor"`
	Category         ProductItemCategory `json:"category"`
	Images           []ProductItemImage  `json:"images"`
}

type ProductItemVendor struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

type ProductItemCategory struct {
	ID    uint                     `json:"id"`
	Name  string                   `json:"name"`
	Child ProductItemCategoryChild `json:"child"`
}

type ProductItemCategoryChild struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

type ProductItemImage struct {
	FileName string `json:"file_name"`
}
