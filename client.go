package catalog_rewards

import (
	"errors"
	"fmt"
)

type Client struct {
	Token   string
	Code    string
	Project string
	URL     string
}

// NewClient returns new Client struct
func NewClient(token string, code string, project, apiUrl string) (*Client, error) {

	if token == "" && code == "" && project == "" && apiUrl == "" {
		return nil, errors.New("token, code, project and url are required to create a Client")
	}

	return &Client{
		Token:   token,
		Code:    code,
		Project: project,
		URL:     apiUrl,
	}, nil
}

// Builds the url of the v1 version to be consumed
func (c *Client) buildURLV1() string {
	return fmt.Sprintf("%s/api/v1/clients/%s/%s", c.URL, c.Code, c.Project)
}

// Builds the url of the v2 version to be consumed
func (c *Client) buildURLV2() string {
	return fmt.Sprintf("%s/api/v2/clients/%s/%s", c.URL, c.Code, c.Project)
}
