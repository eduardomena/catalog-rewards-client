package catalog_rewards

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

// A new request is initialized
func (c *Client) NewRequest(method string, url string, payload interface{}) (*http.Request, error) {

	var buf io.Reader

	if payload != nil {
		b, err := json.Marshal(&payload)
		if err != nil {
			return nil, err
		}

		buf = bytes.NewBuffer(b)
	}

	req, err := http.NewRequest(method, url, buf)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.Token))

	return req, nil
}

// Gets the list of categories that are in the project's catalog.
func (c *Client) GetCategories() (CategoriesResponse, error) {

	categories := CategoriesResponse{}

	url := fmt.Sprintf("%s/categories/", c.buildURLV1())

	req, err := c.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return categories, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return categories, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return categories, err
	}

	json.Unmarshal(body, &categories.Data)

	if res.StatusCode < 200 || res.StatusCode > 299 {
		categories.StatusCode = uint(res.StatusCode)
		return categories, errors.New(string(body))
	}

	return categories, nil
}

// Gets the products that are in the project's catalog.
func (c *Client) GetProducts() (ProductsResponse, error) {

	products := ProductsResponse{}

	url := fmt.Sprintf("%s/products/", c.buildURLV1())

	req, err := c.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return products, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return products, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return products, err
	}

	json.Unmarshal(body, &products.Data)

	if res.StatusCode < 200 || res.StatusCode > 299 {
		products.StatusCode = uint(res.StatusCode)
		return products, errors.New(string(body))
	}

	return products, nil
}

// Gets the information of the products by category that are in the project's catalog.
func (c *Client) GetProductsByCategory(categoryID int) (ProductsResponse, error) {

	products := ProductsResponse{}

	url := fmt.Sprintf("%s/categories/%d/", c.buildURLV1(), categoryID)

	req, err := c.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return products, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return products, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return products, err
	}

	json.Unmarshal(body, &products.Data)

	if res.StatusCode < 200 || res.StatusCode > 299 {
		products.StatusCode = uint(res.StatusCode)
		return products, errors.New(string(body))
	}

	return products, nil
}

// Get information about a product.
func (c *Client) GetProduct(productID uint) (ProductResponse, error) {

	product := ProductResponse{}

	url := fmt.Sprintf("%s/products/%d/", c.buildURLV2(), productID)

	req, err := c.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return product, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return product, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return product, err
	}

	json.Unmarshal(body, &product.Product)

	if res.StatusCode < 200 || res.StatusCode > 299 {
		product.StatusCode = uint(res.StatusCode)
		return product, errors.New(string(body))
	}

	return product, nil
}
